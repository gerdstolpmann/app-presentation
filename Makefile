# make readme:		make bytecode executable
# make readme.opt: 	make native executable
# make clean: 		remove intermediate files
# make CLEAN:           remove intermediate files (recursively)
# make distclean: 	remove any superflous files
# make install
#----------------------------------------------------------------------

LIB = /usr/local/lib/presentation
BIN = /usr/local/bin

.PHONY: presentation.bin
presentation.bin:
	$(MAKE) -f Makefile.code presentation.bin

.PHONY: presentation.bin.opt
presentation.bin.opt:
	$(MAKE) -f Makefile.code presentation.bin.opt

#.PHONY: readme.opt
#readme.opt:
#	$(MAKE) -f Makefile.code readme.opt


.PHONY: clean
clean:
	rm -f *.cmi *.cmo *.cma *.cmx *.o *.a *.cmxa
	rm -f dtd.ml

.PHONY: CLEAN
CLEAN: clean

.PHONY: distclean
distclean: clean
	rm -f *~ depend depend.pkg
	rm -f presentation.bin

.PHONY: install
install:
	mkdir -p $(LIB)
	cp presentation presentation.bin make-headline make-headline-*.scm $(LIB)
	echo "#! /bin/sh" >$(BIN)/presentation
	echo 'exec $(LIB)/presentation "$$@"' >>$(BIN)/presentation
	chmod 775 $(BIN)/presentation

.PHONY: install.opt
install.opt:
	$(MAKE) install
	cp presentation.bin.opt $(LIB)/presentation.bin
