(* $Id$
 * ----------------------------------------------------------------------
 *
 *)

open Pxp_types
open Pxp_document
open Pxp_yacc


let rec print_error e =
  prerr_endline (string_of_exn e)
;;


let run f x =
  try f x with
      e -> print_error e;
	   exit 1
;;


let catalog =
  [ "-//GERD STOLPMANN//DTD PRESENTATION 1.0//EN", Dtd.dtd_1 ] ;;


let parse_document idx tag_map filename =
  let url = Pxp_reader.make_file_url filename in
  let resolver =
    new Pxp_reader.combine
      [ Pxp_reader.lookup_public_id_as_string catalog;
        new Pxp_reader.resolve_as_file();
      ]
  in

  parse_document_entity
    ~id_index: idx
    { default_config with encoding = `Enc_utf8 }
    (ExtID (System (Neturl.string_of_url url), resolver))
    tag_map
;;


let languages_of_doc node =
  let l =
    find_all_elements ~deeply:true "language" node in
  List.map
    (fun n -> n # required_string_attribute "code")
    l
;;


let convert_to_html no_gifs remove_prefix langs filename =
  (* read in style definition *)
  let idx = (new hash_index :> To_html.shared index) in
  let document = parse_document idx To_html.tag_map filename in
  let root = document # root in
  let store = new To_html.store remove_prefix in
  if no_gifs then store # no_gifs;
  root # extension # enumerate_pages 0;
  ignore(root # extension # enumerate_pictures 1);
  let hier = root # extension # collect_hierarchy true idx in
  store # set_hierarchy hier;
  let langs' =
    if langs = [] then
      languages_of_doc root
    else
      langs
  in
  root # extension # print_pages langs' store idx
;;


let convert_to_latex use_ocamldoc front_file langs filename =
  let convert idx (root : To_latex.shared node) lang outlang =
    let dirname = Filename.dirname filename in
    let outname = Filename.chop_extension (Filename.basename filename) ^ 
                  outlang ^ ".tex" in
    let out = open_out outname in
    root # extension # enumerate_pages 0;
    ignore(root # extension # enumerate_pictures 1);
    root # extension # set_dirname dirname;
    root # extension # set_option "use_ocamldoc" (string_of_bool use_ocamldoc);
    ( match front_file with
	  Some f -> root # extension # set_option "front_file" f
	| None -> ()
    );
    let hier = root # extension # collect_hierarchy idx (fun ~lang -> "") in
    root # extension # print_pages out lang idx hier;
    close_out out
  in

  (* read in style definition *)
  let idx = (new hash_index :> To_latex.shared index) in
  let document = parse_document idx To_latex.tag_map filename in
  let root = document # root in
  let langs' =
    if langs = [] then
      languages_of_doc root
    else
      langs
  in
  if langs' = [] then
    (* No I18N *)
    convert idx root `Any ""
  else
    List.iter
      (fun lang -> convert idx root (`Lang lang) ("." ^ lang))
      langs'
;;


let main() =
  let no_gifs = ref false in
  let remove = ref "" in
  let ocamldoc = ref false in
  let front_file = ref None in
  let langs = ref [] in
  let filename = ref None in
  let action = ref (fun filename -> convert_to_html !no_gifs !remove !langs filename) in
  Arg.parse
      [ "-nogifs", Arg.Set no_gifs,
	      "  do not invoke THE GIMP to create icons";
	"-remove", Arg.String (fun s -> remove := s),
                "<prefix>  remove this prefix from all URLs";
	"-latex", Arg.Unit (fun _ -> action := 
			      (fun fn -> convert_to_latex !ocamldoc !front_file !langs fn)),
               "  output LateX code instead of HTML";
	"-ocamldoc", Arg.Set ocamldoc,
	          "  [Latex:] include the ocamldoc package";
	"-front", Arg.String (fun s -> front_file := Some s),
	       "<file>  [Latex:] include this file first";
	"-lang", Arg.String (fun s -> langs := !langs @ [s]),
	       "<ls>  Generate only for language suffix <ls>";
      ]
      (fun s ->
	 match !filename with
	     None -> filename := Some s
	   | Some _ ->
	       raise (Arg.Bad "Multiple arguments not allowed."))
      "usage: presentation [options] input.xml";
  let fn =
    match !filename with
	None ->
	  prerr_endline "presentation: no input";
	  exit 1
      | Some s -> s
  in
  run !action fn
;;

main();;
