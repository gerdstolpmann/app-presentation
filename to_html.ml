(* $Id$
 * ----------------------------------------------------------------------
 *
 *)


open Pxp_types
open Pxp_document
open Pxp_yacc
open Printf

let map'n'concat f l =
  String.concat "" (List.map f l)
;;

let escape_html_re = (Pcre.regexp "\\<|\\>|\\&|\\\"|\\@|\\:");;

let escape_html s =
  let s' =
    Pcre.substitute
      ~rex:escape_html_re
      ~subst:
        (fun s ->
           match s with
               "<" -> "&lt;"
             | ">" -> "&gt;"
             | "&" -> "&amp;"
             | "\"" -> "&quot;"
	     | "@" -> "&#64;"  (* protect mail addresses (anti-spam) *)
	     | ":" -> "&#58;"  (* protect mailto links (anti-spam) *)
             | _ -> assert false)
      s
  in
  Netconversion.convert
    ~in_enc:`Enc_utf8
    ~out_enc:`Enc_usascii
    ~subst:(fun k -> "&#" ^ string_of_int k ^ ";")
    s'
;;


let escape_quotes s =
  Pcre.substitute
    ~rex:(Pcre.regexp "[\\\"\\\\]")
    ~subst:
      (fun s ->
	 match s with
             "\"" -> "\\\""
	   | "\\" -> "\\\\"
	   | _    -> assert false)
    s
;;


let run script_name args =
  let p =
    try Sys.getenv "PRESENTATION"
    with
	Not_found -> "."
  in
  let pid = Unix.create_process
	      (p ^ "/" ^ script_name)
	      (Array.append [| script_name |] args)
	      Unix.stdin
	      Unix.stdout
	      Unix.stderr in
  let _, status = Unix.waitpid [] pid in
  match status with
      Unix.WEXITED 0 ->
	()
    | Unix.WEXITED n ->
	failwith ("Script " ^ script_name ^ " exits with error code")
    | Unix.WSIGNALED n ->
	failwith ("Script " ^ script_name ^ " exits on signal")
    | Unix.WSTOPPED n ->
	failwith ("Script " ^ script_name ^ " exits with error code")
;;


type lang =
    [ `Any | `Lang of string ]

type lang_descr =
    { lang_code : string;
      lang_description : string;
      lang_image : string option;
    }


type page_hierarchy =
    { pageid : string;
      pagelink : lang:lang -> string;
      title : lang:lang -> string;
      children : page_hierarchy list;
      related : (string * string * lang) list;
      navstart : bool;
      lyprefix : string;
    }
;;


let string_of_lang =
  function
    | `Any -> "any"
    | `Lang s -> s


class type footnote_printer =
  object
    method footnote_to_html : lang:lang -> store_type -> string
  end

and store_type =
object
    method set_languages : lang_descr list -> unit
    method languages : lang_descr list   (* [] when I18N is disabled *)
    method alloc_footnote : footnote_printer -> int
    method print_footnotes : lang:lang -> unit -> string
    method add_msg : lang:lang -> string -> string -> unit
    method find_msg : lang:lang -> string -> string
    method add_layout : string -> string -> unit
    method find_layout : string -> string  (* or Not_found *)
    method have_layout : string -> bool
    method instantiate_layout : lang:lang -> 
                                string -> (string * string Lazy.t) list
                                  -> string
    method instantiate_layout_text : lang:lang ->
                                     string -> (string * string Lazy.t) list
                                       -> string
    method add_headline_layout :
      string -> (string*string*string*string*string*string*string) -> unit
    method get_headline_layout :
      string -> (string*string*string*string*string*string*string)
    method get_gif : string -> string -> string
    method no_gifs : unit

    method set_hierarchy : page_hierarchy -> unit
    method hierarchy : page_hierarchy

    method url : string -> string

      method first_id : string
      method last_id : string
      method next_id : string -> string option
      method prev_id : string -> string option


      method update_iconindex : unit -> unit
  end
;;


type find_id =
    Not_in_tree
  | Next_node
  | Found of string
;;


(* The default page layout does neither support navigation trees nor related links *)

let std_layout_decls = Hashtbl.create 21;;
Hashtbl.add std_layout_decls "page" "<html><body>@CHILDREN@@FOOTER@</body></html>\n";
Hashtbl.add std_layout_decls "sect1" "<h1>@TITLE@</h1>\n@CHILDREN@";
Hashtbl.add std_layout_decls "sect2" "<h2>@TITLE@</h2>\n@CHILDREN@";
Hashtbl.add std_layout_decls "sect3" "<h3>@TITLE@</h3>\n@CHILDREN@";
Hashtbl.add std_layout_decls "p" "<p>@CHILDREN@</p>";
Hashtbl.add std_layout_decls "br" "<br>\n";
Hashtbl.add std_layout_decls "em" "<b>@CHILDREN@</b>";
Hashtbl.add std_layout_decls "ul" "<ul>@CHILDREN@</ul>";
Hashtbl.add std_layout_decls "li" "<li>@CHILDREN@</li>";
Hashtbl.add std_layout_decls "code" "<p><pre>@CHILDREN@</pre></p>";
Hashtbl.add std_layout_decls "c" "<tt>@CHILDREN@</tt>";
Hashtbl.add std_layout_decls "a.emptyref" "@CHILDREN@";
Hashtbl.add std_layout_decls "a.default" "<a href=\"@HREF@\">@CHILDREN@</a>";
Hashtbl.add std_layout_decls "numref.emptyref" "@CHILDREN@";
Hashtbl.add std_layout_decls "numref.default" "<a href=\"@HREF@\">@NUMBER@</a>";
Hashtbl.add std_layout_decls "nameref.emptyref" "@CHILDREN@";
Hashtbl.add std_layout_decls "nameref.default" "<a href=\"@HREF@\">@TITLE@</a>";
Hashtbl.add std_layout_decls "footer" "<hr align=left noshade=noshade width='30%'><dl>@FOOTNOTES@</dl>";
Hashtbl.add std_layout_decls "footer.empty" "";
Hashtbl.add std_layout_decls "footnote.text" "<a name=\"@TEXTANCHOR@\" href=\"#@FOOTANCHOR@\">[@SYMBOL@]</a>";
Hashtbl.add std_layout_decls "footnote.foot" "<dt><a name=\"@FOOTANCHOR@\" href=\"#@TEXTANCHOR@\">[@SYMBOL@]</a></dt><dd>@CHILDREN@</dd>";
(* The following decls are for backward compatibility: *)
Hashtbl.add std_layout_decls "navigator.start" "@SUBLEVELS@";
Hashtbl.add std_layout_decls "navigator.onpath" "@navigator.level@";
Hashtbl.add std_layout_decls "navigator.topurl" "";
Hashtbl.add std_layout_decls "navigator.toptitle" "";
Hashtbl.add std_layout_decls "langselect" "<ul>@LINK@</ul>";
Hashtbl.add std_layout_decls "langselect.link" "<li><a href=\"@HREF@\">@TEXT@</a></li>";
;;


let msg_re = Pcre.regexp "^MSG:(.*)$";;

class store remove_this_url_prefix : store_type =
  object (self)

    val mutable languages = []

    val mutable footnotes = ( [] : (int * footnote_printer) list )
    val mutable next_footnote_number = 1

    val mutable messages = ( Hashtbl.create 21 : (lang * string, string) Hashtbl.t)

    val mutable layout_decls = ( Hashtbl.create 21 : (string, string) Hashtbl.t )
    val mutable headline_decls = []

    val mutable gif_files = ( [] : ((string * string) * string) list )
    val mutable next_gif_number = 0
    val mutable create_gifs = true

    val mutable pictures = ( [] : (string * int) list )
    val mutable next_pic_number = 1

    val mutable hier = ( None : page_hierarchy option )
    val mutable hier_list = ( [] : string list )

    val mutable remove_url_prefix = (remove_this_url_prefix : string)

    initializer
      if Sys.file_exists "iconindex" then begin
	let f = open_in_bin "iconindex" in
	let (g,n) = input_value f in
	gif_files <- g;
	next_gif_number <- n;
	close_in f
      end


    method set_languages l =
      languages <- l

    method languages = 
      languages


    method add_msg ~lang name s =
      Hashtbl.replace messages (lang,name) s

    method find_msg ~lang name =
      match lang with
	| `Any ->
	    Hashtbl.find messages (`Any,name)
	| `Lang _ ->
	    ( try Hashtbl.find messages (lang,name)
	      with
		  Not_found -> Hashtbl.find messages (`Any,name)
	    )


    method url s =
      let p = String.length remove_url_prefix in
      let l = String.length s in
      if p < l && String.sub s 0 p = remove_url_prefix then
	String.sub s p (l-p)
      else
	s

    method set_hierarchy h =
      let rec flatten h =
	h.pageid :: (List.flatten (List.map flatten h.children))
      in
      hier <- Some h;
      hier_list <- flatten h;


    method hierarchy =
      match hier with
	  None -> failwith "hierarchy"
	| Some h -> h

    method first_id =
      List.hd hier_list

    method last_id =
      List.hd (List.rev hier_list)

    method prev_id id =
      let rec find prev l =
	match l with
	    [] -> 
	      None
	  | x :: l' ->
	      if x = id then
		prev
	      else
		find (Some x) l'
      in
      find None hier_list


    method next_id id =
      let rec find l =
	match l with
	    [] ->
	      None
	  | [x] ->
	      None
	  | x :: y :: l' ->
	      if x = id then
		Some y
	      else
		find (y :: l')
      in
      find hier_list


    method alloc_footnote n =
      let number = next_footnote_number in
      next_footnote_number <- number+1;
      footnotes <- footnotes @ [ number, n ];
      number


    method print_footnotes ~lang () =
      if footnotes =  [] then
	self # instantiate_layout ~lang "footer.empty" []
      else begin
	let all_footnotes() =
	  map'n'concat
	    (fun (_,n) ->
	       n # footnote_to_html ~lang (self : #store_type :> store_type))
	    footnotes
	in

	let f =
	  self # instantiate_layout ~lang "footer"
	    [ "FOOTNOTES", lazy (all_footnotes()) ] in
	footnotes <- [];
	next_footnote_number <- 1;
	f
      end


    method add_layout name text =
      if Hashtbl.mem layout_decls name then
	failwith ("Layout '" ^ name ^ "' defined twice");
      Hashtbl.replace layout_decls name text;


    method find_layout name =
      try
	Hashtbl.find layout_decls name
      with
	  Not_found ->
	    Hashtbl.find std_layout_decls name

    method have_layout name =
      try ignore(self # find_layout name); true with Not_found -> false

    method instantiate_layout_text ~lang text bindings =
      Pcre.substitute
	~rex:(Pcre.regexp "\\@[^\\@\n]*\\@")
	~subst:(fun s ->
	   let n = String.sub s 1 (String.length s - 2) in
	   if n = "" then
	     "@"
	   else
	     if n = "LANG" then
	       string_of_lang lang
	     else
	       try
		 let r = Pcre.exec ~rex:msg_re n in  (* or Not_found *)
		 let msg_name = Pcre.get_substring r 1 in
		 ( try
		     self # find_msg ~lang msg_name
		   with
		       Not_found ->
			 failwith ("Message not found: " ^ msg_name)
		 )
	       with
		   Not_found ->
		     ( try Lazy.force (List.assoc n bindings)
		       with
			   Not_found ->
			     begin try
			       let subst = self # find_layout n in
			       self # instantiate_layout_text 
				 ~lang subst bindings
			     with
				 Not_found ->
				   failwith ("Layout not found: " ^ n)
			     end
		     )
	       )
	text


    method instantiate_layout ~lang name bindings =
      try
	let subst = self # find_layout name in
	self # instantiate_layout_text ~lang subst bindings
      with
	  Not_found ->
	    failwith ("Layout not found: " ^ name)


    method add_headline_layout name ( ((font:string),
				       font_size,text_color,high_color,
				       side_color,shadow_color,bg_color) as spec
				    ) =
      let color_re = Pcre.regexp "[0-9a-fA-F][0-9a-fA-F][0-9a-fA-F][0-9a-fA-F][0-9a-fA-F][0-9a-fA-F]" in
      List.iter
	(fun c ->
	   if not (Pcre.pmatch ~rex:color_re c) then
	     failwith ("Bad color specification: " ^ c))
	[ text_color; high_color; side_color; shadow_color; bg_color ];
      begin
	try ignore(int_of_string font_size)
	with
	    _ -> failwith ("Bad font size: " ^ font_size)
      end;
      headline_decls <- (name, spec) :: headline_decls


    method get_headline_layout name =
      List.assoc name headline_decls


    method get_gif text props =
      try
	List.assoc (text,props) gif_files
      with
	  Not_found ->
	    (* The GIF does not exist yet. Let's create it on the fly: *)
	    let n = next_gif_number in
	    next_gif_number <- n + 1;
	    let filename = "icon-" ^ string_of_int n ^ ".gif" in
	    let (font,font_size,text_color,high_color,side_color,shadow_color,
		 bg_color) =
	      try self # get_headline_layout props
	      with
		  Not_found ->
		    failwith ("Headline layout not defined: " ^ props)
	    in
	    if create_gifs then
	      run "make-headline" [| escape_quotes text;
				     escape_quotes filename;
				     escape_quotes font;
				     font_size;
				     text_color;
				     high_color;
				     side_color;
				     shadow_color;
				     bg_color;
				  |];
	    gif_files <- ((text,props), filename) :: gif_files;
	    if create_gifs then
	      self # update_iconindex ();
	    filename

    method no_gifs =
      create_gifs <- false

    method update_iconindex () =
      let f = open_out_bin "iconindex" in
      output_value f (gif_files, next_gif_number);
      close_out f

  end
;;


class virtual shared =
  object (self)

    (* --- default_ext --- *)

    val mutable node = (None : shared node option)

    method clone = {< >}
    method node =
      match node with
          None ->
            assert false
        | Some n -> n
    method set_node n =
      node <- Some n


    (* --- for all --- *)

    (* First, 'enumerate_pages' is invoked on all 'layout' and 'page' objects.
     * All preprocessing should happen here. After that, 'print_pages' is
     * invoked on all 'layout' and 'page' objects. The latter will
     * subsequently invoke 'to_html' to compute the page contents.
     * 'print_pages' prints the pages in all selected languages.
     *)

    method enumerate_pages (n : int) =
      ()

    method enumerate_pictures (n : int) =
      (* Iterates over all elements: *)
      List.fold_left
	(fun n0 x ->
	   x # extension # enumerate_pictures n0)
	n
      	(self # node # sub_nodes)

    method print_pages (langs : string list) (s : store) (idx:shared index) =
      (* langs = []: Disable I18N. This must be a subset of s#languages *)
      ()

    method collect_hierarchy (navstart_dfl : bool) (idx : shared index) =
      (failwith "collect_hierarchy" : page_hierarchy)

    (* --- special methods --- *)

    method url_of_object (_ : lang) =  (* only for objects with ID attribute *)
      (failwith "url_of_object" : string)

    method url_of_object_nolang =
      (failwith "url_of_object" : string)
	(* Without language suffix *)

    method toc_title_of_object (_ : lang) =
      (failwith "toc_title_of_object" : string)

    method object_number =
      (failwith "object_number" : string)

    (* --- auxiliary --- *)

    method get_page =
      (* Get the page containing this node *)
      let p = ref (self # node) in
      begin
	try
	  while !p # node_type <> T_element "page" do
	    p := !p # parent;
	  done
	with
	    Not_found -> assert false
      end;
      !p

    (* --- virtual --- *)

    method virtual to_html : lang:lang -> store -> (shared index) -> string

  end
;;


class only_data =
  object (self)
    inherit shared

    method to_html ~lang store idx =
      escape_html (self # node # data)
  end
;;


class html =
  object (self)
    inherit shared

    method to_html ~lang store idx =
      self # node # data
  end
;;


class latex =
  object (self)
    inherit shared

    method to_html ~lang store idx =
      ""
  end
;;


class no_markup =
  object (self)
    inherit shared

    method to_html ~lang store idx =
      map'n'concat
	(fun n -> n # extension # to_html ~lang store idx)
	(self # node # sub_nodes)
  end
;;


class empty =
  object (self)
    inherit shared

    method to_html ~lang store idx =
      ""
  end
;;


class presentation =
  object (self)
    inherit no_markup

    method enumerate_pages n =
      let rec enum k l =
	match l with
	    [] -> ()
	  | p :: l' ->
	      let k' =
		if p # node_type = T_element "page" then k+1 else k in
	      p # extension # enumerate_pages k;
	      enum k' l'
      in
      enum 0 (self # node # sub_nodes)

    method print_pages langs store idx =
      List.iter
	(fun n -> n # extension # print_pages langs store idx)
	(self # node # sub_nodes)

    method collect_hierarchy navstart_dfl idx =
      let rec find_hier l =
	match l with
	    [] -> assert false
	  | x :: l' ->
	      if x # node_type = T_element "hierarchy" then
		x # extension # collect_hierarchy navstart_dfl idx
	      else
		find_hier l'
      in
      find_hier (self # node # sub_nodes)

  end
;;


class languages =
  object (self)
    inherit shared

    method to_html ~lang store idx =
      ""

    method print_pages langs store idx =
      let langs = 
	List.map
	  (fun n ->
	     assert( n # node_type = T_element "language" );
	     { lang_code = n # required_string_attribute "code";
	       lang_description = n # data;
	       lang_image = n # optional_string_attribute "image"
	     }
	  )
	  self # node # sub_nodes in
      store # set_languages langs

  end
;;


class msg =
  object (self)
    inherit shared

    method to_html ~lang store idx =
      ""

    method print_pages langs store idx =
      let name =
	self # node # required_string_attribute "name" in
      let lang =
	self # node # required_string_attribute "lang" in
      store # add_msg ~lang:(`Lang lang) name (self # node # data);

  end
;;


class layout =
  object (self)
    inherit shared

    method to_html ~lang store idx =
      ""

    method print_pages langs store idx =
      let name =
	match self # node # attribute "name" with
	    Value s -> s
	  | _ -> assert false
      in
      store # add_layout name (self # node # data);

  end
;;


class headline_layout =
  object (self)
    inherit shared

    method to_html ~lang store idx =
      ""

    method print_pages langs store idx =
      let [ name; font; font_size; text_color; high_color; side_color;
	    shadow_color; bg_color ] =
	List.map
	  (fun attname ->
	     match self # node # attribute attname with
		 Value s -> s
	       | _ -> assert false)
	  [ "name"; "font"; "font-size"; "text-color"; "high-color";
	    "side-color"; "shadow-color"; "bg-color" ]
      in
      store # add_headline_layout
	name
	(font,font_size,text_color,high_color,side_color,shadow_color,bg_color)
  end
;;


class hierarchy =
  object (self)
    inherit shared

    method to_html ~lang store idx = ""

    method collect_hierarchy navstart_dfl idx =
      let navstart_dfl' =
	match self # node # optional_string_attribute "navstart" with
	    Some "yes" -> true
	  | Some _ -> false
	  | None -> navstart_dfl in
      (List.hd (self # node # sub_nodes)) # extension # 
	collect_hierarchy navstart_dfl' idx
  end
;;


class related =
  object (self)

    inherit shared

    method to_html ~lang store idx =
      escape_html (self # node # data)

  end
;;


class plevel =
  object (self)
    inherit shared

    method to_html ~lang store idx = ""

    method collect_hierarchy navstart_dfl idx =
      let navstart =
	match self # node # optional_string_attribute "navstart" with
	    Some "yes" -> true
	  | Some _ -> false
	  | None -> navstart_dfl in
      let lyprefix =
	match self # node # optional_string_attribute "layout-prefix" with
	    Some p -> p
	  | None -> "navigator" in
      let pageid =
	match self # node # attribute "idref" with
	    Value s -> s
	  | _ -> assert false
      in
      let page =
	try
	  idx # find pageid
	with
	    Not_found ->
	      failwith ("Cannot find object with ID: " ^ pageid)
      in
      let pagelink = page # extension # url_of_object in
      let title    = page # extension # toc_title_of_object in
      let children =
	List.flatten
	  (List.map
	     (fun n ->
		if n # node_type = T_element "plevel" then
		  [n # extension # collect_hierarchy navstart_dfl idx]
		else
		  []
	     )
	     (self # node # sub_nodes)) in
      let related =
	List.flatten
	  (List.map
	     (fun n ->
		if n # node_type = T_element "related" then
		  let href =
		    match n # attribute "href" with
			Value s -> s
		      | Implied_value -> ""
		      | _ -> assert false in
		  let idref =
		    match n # attribute "idref" with
			Value s -> s
		      | Implied_value -> ""
		      | _ -> assert false in
		  let lang =
		    match n # attribute "lang" with
			Value s -> (`Lang s)
		      | Implied_value -> `Any
		      | _ -> assert false in
		  let hlink =
		    if idref <> "" then
		      (idx # find idref) # extension # url_of_object lang
		    else
		      if href <> "" then
			href
		      else
			"UNKNOWN"  in

		  [ escape_html (n # data), hlink, lang ]
		  (* This is not optimal. Either store n directly (difficult
		   * to type check), or invoke # to_html, but we have no store
		   * here.
		   *)
		else
		  []
	     )
	     (self # node # sub_nodes)) in

      { pageid = pageid;
	pagelink = (fun ~lang -> pagelink lang);
	title = (fun ~lang -> title lang);
	children = children;
	related = related;
	navstart = navstart;
	lyprefix = lyprefix;
      }
  end
;;


let title_of_node ~lang n =
  let rec find_title best_title l =
    match l with
      | n :: l' when n # node_type = (T_element "title") ->
	  ( match n # optional_string_attribute "lang" with
	      | None -> find_title n#data l'
	      | Some t ->
		  if (`Lang t) = lang then
		    n#data
		  else
		    find_title best_title l'
	  )
      | _ -> best_title
  in
  match n # optional_string_attribute "title" with
    | None ->
	find_title "" n#sub_nodes
    | Some t ->
	find_title t n#sub_nodes
;;


exception Skip_page

class page =
  object (self)
    inherit shared

    val mutable this_page = (-1)

    method to_html ~lang store idx =
      try
	(* output header *)
	let title = title_of_node ~lang self#node in
	let id =
	  match self # node # attribute "id" with
	      Value s -> s
	    | Implied_value -> "#IMPLIED"
	    | _ -> assert false
	in
	(* process main content: *)
	let main =
          map'n'concat
	    (fun n -> n # extension # to_html ~lang store idx)
	    (self # node # sub_nodes) in
	
      (* now process footnotes *)
	let footer() = store # print_footnotes ~lang () in
	
	let rec locate_current_page rev_path tree =
	  (* Returns path from current page to root page *)
	  if tree.pageid = id then
	    tree :: rev_path
           else
	    locate_current_page_in_list (tree :: rev_path) tree.children
	  
         and locate_current_page_in_list rev_path tree_list =
	  match tree_list with
	    | [] -> raise Not_found
	    | t :: tree_list' ->
		try 
		  locate_current_page rev_path t
		with
		    Not_found ->
		      locate_current_page_in_list rev_path tree_list'
	in
	
	let hierarchy = store # hierarchy in
	
	let top_url = store # instantiate_layout "navigator.topurl" [] in
	let top_title = store # instantiate_layout "navigator.toptitle" [] in
	
	let rev_path_to_root =
	  try
	    locate_current_page [] hierarchy @
	    [ { pageid = "#TOP";     (* always append this pseudo node *)
		pagelink = top_url;
		title = top_title;
		children = [ hierarchy ];
		related = [];
		navstart = true;
		lyprefix = "navigator";
	      }
	    ]
	  with
	      Not_found -> 
		prerr_endline("Warning: Page does not occur in hierarchy: " ^ id);
		raise Skip_page
	in


	let parent_page =
	  List.hd (List.tl rev_path_to_root) in

	let this_page =
	  List.hd rev_path_to_root in

	let start_page0 =
	  List.find (fun p -> p.navstart) (List.tl rev_path_to_root) in

	let start_page =
	  if start_page0.pageid = "#TOP" then this_page else start_page0 in

	let start_parent_page =  (* the parent of start_page *)
	  match
	    List.fold_left 
	      (fun acc p -> 
		 match acc with
		   | `None -> if p.pageid = start_page.pageid then `Start p else `None
		   | `Start q -> `Parent p
		   | `Parent q -> acc)
	      `None
	      rev_path_to_root
	  with
	      `None -> assert false
	    | `Start p -> assert false
	    | `Parent p -> p
	in
	
	let lyname page =
	  let suffix =
	    if page.pageid = this_page.pageid then
	      "current"
	    else
	      if List.exists (fun p -> p.pageid = page.pageid) rev_path_to_root then
		"onpath"
	      else
		"level" in
	  page.lyprefix ^ "." ^ suffix in

	let lyname_child page =
	  let name = page.lyprefix ^ ".child" in
	  if store # have_layout name then name else lyname page in

	let rec expand_for_page parent page =
	  let ly = 
	    if parent.pageid = this_page.pageid then lyname_child page else lyname page in
	  store # instantiate_layout ly
	    ~lang
	    [ "LEVELTITLE", lazy (escape_html (page.title ~lang));
	      "LEVELURL",   lazy (escape_html (store # url (page.pagelink ~lang)));
	      "UPTITLE",    lazy (escape_html (parent.title ~lang));
	      "UPURL",      lazy (escape_html (store # url (parent.pagelink ~lang)));
	      "SUBLEVELS",  lazy (expand_sublevels_for_page page);
	    ]
	and expand_sublevels_for_page page =
	  map'n'concat
	    (fun child -> expand_for_page page child)
	    page.children
	in

	let levels() =
	  let ly = 
	    if start_page.pageid = this_page.pageid && store#have_layout "navigator.root"
	    then "navigator.root"
	    else "navigator.start" in
	  store # instantiate_layout ly
	    ~lang
	    [ "LEVELTITLE", lazy (escape_html (start_page.title ~lang));
	      "LEVELURL",   lazy (escape_html (store # url (start_page.pagelink ~lang)));
	      "UPTITLE",    lazy (escape_html (start_parent_page.title ~lang));
	      "UPURL",      lazy (escape_html (store # url (start_parent_page.pagelink ~lang)));
	      "SUBLEVELS",  lazy (expand_sublevels_for_page start_page)
	    ]
	in
	
	let related() =
	  if this_page.related <> [] then begin
	    let link() =
	      map'n'concat
		(fun (text,href,rlang) ->
		   if rlang = `Any || rlang = lang then
		     store # instantiate_layout "related.link"
		       ~lang
		       [ "TEXT", lazy text;
			 "HREF", lazy (escape_html (store # url href));
		       ]
		   else ""
		)
		this_page.related in
	    store # instantiate_layout "related"
	      ~lang
	      [ "LINK", lazy (link());
	      ]
	  end else ""
	in

	let langselect() =
	  if store # languages <> [] then (
	    let link() =
	      map'n'concat
		(fun l ->
		   store # instantiate_layout "langselect.link"
		     ~lang
		     [ "CODE", lazy l.lang_code;
		       "TEXT", lazy l.lang_description;
		       "IMGURL", lazy (match l.lang_image with
					 | None -> ""
					 | Some u ->
					     escape_html (store # url u));
		       "HREF", (lazy (escape_html
					(store # url
					   (self # url_of_object 
					      (`Lang l.lang_code)))));
		     ]
		)
		store#languages in
	    store # instantiate_layout "langselect"
	      ~lang
	      [ "LINK", lazy (link());
	      ]
	  )
	  else ""
	in

	let prev_url, prev_title =
	  match store # prev_id id with
	      None -> ("", "")
	    | Some prev_id ->
		( try
		    let prev_page = idx # find prev_id in
		    (prev_page # extension # url_of_object lang,
		     prev_page # extension # toc_title_of_object lang)
		  with
		      Not_found -> ("", "")
		) in

	let prev_link =
	  if prev_url <> "" then
	    "<LINK REL=\"Prev\" HREF=\"" ^
	    escape_html prev_url ^
	    "\" TITLE=\"" ^
	    escape_html prev_title ^
	    "\">"
	  else ""
	in

	let next_url, next_title =
	  match store # next_id id with
	      None -> ("", "")
	    | Some next_id ->
		( try
		    let next_page = idx # find next_id in
		    (next_page # extension # url_of_object lang,
		     next_page # extension # toc_title_of_object lang)
		  with
		      Not_found -> ("", "")
		) in
	
	let next_link =
	  if next_url <> "" then 
	    "<LINK REL=\"Next\" HREF=\"" ^
	    escape_html next_url ^
	    "\" TITLE=\"" ^
	    escape_html next_title ^ 
	    "\">"
	  else ""
	in
	
	let first_url, first_title =
	  match store # first_id with
	    | first_id ->
		( try
		    let first_page = idx # find first_id in
		    (first_page # extension # url_of_object lang,
		     first_page # extension # toc_title_of_object lang)
		  with
		      Not_found -> ("", "")
		) in

	let last_url, last_title =
	  match store # last_id with
	    | last_id ->
		( try
		    let last_page = idx # find last_id in
		    (last_page # extension # url_of_object lang,
		     last_page # extension # toc_title_of_object lang)
		  with
		      Not_found -> ("", "")
		) in

	let page_text =
	  store # instantiate_layout "page"
	    ~lang
	    [ "TITLE",    lazy (escape_html title);
	      "UPURL",    lazy (escape_html (store # url (parent_page.pagelink ~lang)));
	      "UPTITLE",  lazy (escape_html (parent_page.title ~lang));
	      "PREVLINK", lazy (prev_link);
	      "PREVURL",  lazy (prev_url);
	      "PREVTITLE",lazy (prev_title);
	      "NEXTLINK", lazy (next_link);
	      "NEXTURL",  lazy (next_url);
	      "NEXTTITLE",lazy (next_title);
	      "FIRSTURL", lazy (first_url);
	      "FIRSTTITLE", lazy (first_title);
	      "LASTURL",  lazy (last_url);
	      "LASTTITLE", lazy (last_title);
	      "LEVEL",    lazy (levels());
	      "NAVIGATION", lazy (levels());
	      "RELATED",  lazy (related());
	      "LANGSELECT", lazy (langselect());
               "FOOTER",   lazy (footer());
	      "CHILDREN", lazy main;
	    ]
	in
	page_text
      with
	  Skip_page -> ""

    method toc_title_of_object lang =
      title_of_node ~lang self#node

    method enumerate_pages n =
      this_page <- n

    method object_number =
      assert(this_page >= 0);
      string_of_int this_page

    method print_pages langs store idx =
      (* I18N processing is done when langs<>[]:
       * - Process all these languages in turn
       *
       * I18N means we have to write the following files:
       * - A type map file
       * - A HTML file for every language
       *)
      if langs = [] then (
	(* No I18N! *)
	let filename = self # url_of_object `Any in
	let f = open_out filename in
	try
	  output_string f (self # to_html ~lang:`Any store idx);
	  close_out f
	with
	    any ->
	      close_out f;
	      raise any
      )
      else (
	(* First write the type map file: *)
	let base_name = Filename.chop_extension (self # url_of_object_nolang) in
	let tmap_name = base_name ^ ".var" in
	let f = open_out tmap_name in
	( try
	    fprintf f "URI: %s\n\n" base_name;
	    List.iter
	      (fun l ->
		 let u = self # url_of_object (`Lang l) in
		 fprintf f "URI: %s\n" u;
		 fprintf f "Content-Language: %s\n\n" l
	      )
	      langs;
	    close_out f;
	  with any -> close_out f; raise any
	);

	(* Then generate the page for every language in turn: *)
	List.iter
	  (fun l ->
	     let filename = self # url_of_object (`Lang l) in
	     let f = open_out filename in
	     try
	       output_string f (self # to_html ~lang:(`Lang l) store idx);
	       close_out f
	     with
		 any ->
		   close_out f;
		   raise any
	  )
	  langs;
      )

    method url_of_object_nolang =
      match self # node # attribute "file" with
	  Value s -> s
	| Implied_value -> "page-" ^ string_of_int this_page ^ ".html"
	| _ -> assert false

    method url_of_object lang =
      let base_name = self # url_of_object_nolang in
      match lang with
	| `Any ->
	    base_name
	| `Lang l ->
	    base_name ^ "." ^ l

  end
;;


class section the_tag =
  object (self)
    inherit shared

    val tag = the_tag

    method private language =
      (* Determine the language of the element *)
      match self # node # optional_string_attribute "lang" with
	| None -> `Any
	| Some l -> `Lang l

    method to_html ~lang store idx =
      let taglang = self # language in
      if taglang = `Any || taglang = lang then (
	let title = title_of_node ~lang self # node in
	let children =
	  map'n'concat
	    (fun n -> n # extension # to_html ~lang store idx)
	    self#node#sub_nodes
	in
	let text =
	  store # instantiate_layout tag
	    ~lang
	    [ "TITLE", lazy title;
	      "CHILDREN", lazy children;
	      "GIFURL", lazy (escape_html(store # get_gif title tag))
	    ]
	in
	text
      )
      else ""
  end
;;

class sect1 = section "sect1";;
class sect2 = section "sect2";;
class sect3 = section "sect3";;


class map_tag the_tag =
  object (self)
    inherit shared

    val tag = the_tag

    method private language =
      (* Determine the language of the element *)
      match self # node # optional_string_attribute "lang" with
	| None -> `Any
	| Some l -> `Lang l

    method to_html ~lang store idx =
      let taglang = self # language in
      if taglang = `Any || taglang = lang then (
	let children =
	  map'n'concat
	    (fun n -> n # extension # to_html ~lang store idx)
	    (self # node # sub_nodes)
	in
	let text =
	  store # instantiate_layout tag
	    ~lang
	    [ "CHILDREN", lazy children;
	    ]
	in
	text
      )
      else ""
  end
;;

class p = map_tag "p";;
class em = map_tag "em";;
class ul = map_tag "ul";;
class li = map_tag "li";;
class br = map_tag "br";;


class code =
  object (self)
    inherit shared

    method to_html ~lang store idx =
      let data = self # node # data in
      (* convert tabs *)
      let l = String.length data in
      let rec preprocess i column =
	(* this is very ineffective but comprehensive: *)
	if i < l then
	  match data.[i] with
	      '\t' ->
		let n = 8 - (column mod 8) in
		String.make n ' ' ^ preprocess (i+1) (column + n)
	    | '\n' ->
		"\n" ^ preprocess (i+1) 0
	    | c ->
		String.make 1 c ^ preprocess (i+1) (column + 1)
	else
	  ""
      in
      store # instantiate_layout "code"
	~lang
	[ "CHILDREN", lazy (escape_html (preprocess 0 0)) ]

  end
;;

class c =
  object (self)
    inherit shared

    method to_html ~lang store idx =
      let data = self # node # data in
      (* convert tabs and spaces *)
      let l = String.length data in
      let rec preprocess i =
	(* this is very ineffective but comprehensive: *)
	if i < l then
	  match data.[i] with
	      '\t' | ' ' ->
		"&nbsp;" ^ preprocess (i+1)
	    | '\n' ->
		"<br>" ^ preprocess (i+1)
	    | c ->
		escape_html (String.make 1 c) ^ preprocess (i+1)
	else
	  ""
      in
      store # instantiate_layout "c"
	~lang
	[ "CHILDREN", lazy (preprocess 0) ]

  end
;;


class a =
  object (self)
    inherit shared

    method to_html ~lang store idx =
      let langref =
	match self # node # optional_string_attribute "langref" with
	  | None -> lang
	  | Some l -> (`Lang l)
      in
      let href =
	match self # node # attribute "href" with
	    Value v -> escape_html (store # url v)
	  | Valuelist _ -> assert false
	  | Implied_value ->
	      begin match self # node # attribute "idref" with
		  Value v ->
		    let page =
		      try idx # find v
		      with
			  Not_found ->
			    failwith ("Cannot find object with ID: " ^ v)
		    in
		    escape_html (store # url (page # extension # url_of_object langref))
		| Valuelist _ -> assert false
		| Implied_value ->
		    (* Link to this page: *)
		    let p = self # get_page in
		    escape_html (store # url (p # extension # url_of_object langref))
	      end
      in
      let a_type =
	if href="" then
	  "a.emptyref"
	else
	  "a." ^ self # node # required_string_attribute "type" in
      store # instantiate_layout a_type
	~lang
	[ "HREF", lazy href;
	  "CHILDREN", lazy (escape_html (self # node # data))
	]

  end
;;


class picture =
  object (self)
    inherit shared

    val mutable picture_nr = (-1)     (* not yet specified *)

    method object_number =
      assert( picture_nr >= 1);
      string_of_int picture_nr

    method url_of_object lang =
      let page = self # get_page in
      let page_url = page # extension # url_of_object lang in
      page_url ^ "#pic" ^  string_of_int picture_nr

    method private language =
      (* Determine the language of the picture *)
      match self # node # optional_string_attribute "lang" with
	| None -> `Any
	| Some l -> `Lang l

    method toc_title_of_object lang =
      self # node # required_string_attribute "caption"

    method enumerate_pictures n =
      picture_nr <- n;
      n + 1

    method to_html ~lang store idx =
      let rec transform_map f =
	try
	  let line = input_line f in
	  let fields = Pcre.split line in
	  match fields with
	      [] -> transform_map f
	    | [ idref; shape; coords ] ->
		let page =
		  try idx # find idref
		  with
		      Not_found ->
			failwith ("Cannot find object with ID: " ^ idref)
		in
		let href = 
		  escape_html (store # url (page # extension # url_of_object lang))
		in
		let alt = 
		  escape_html (page # extension # toc_title_of_object lang) in
		let s =
		  Printf.sprintf "<AREA HREF=\"%s\" ALT=\"%s\" SHAPE=\"%s\" COORDS=\"%s\">\n"
		    href alt shape coords in
		s ^ transform_map f
		  
	    | _ ->
		failwith ("Bad line in mapfile: "  ^ line)
	with
	    End_of_file ->
	      ""
      in

      let plang = self # language in 
      if plang = `Any || plang = lang then (
	let src     = self # node # required_string_attribute "src" in
	let caption = self # node # required_string_attribute "caption" in
	let map     = self # node # optional_string_attribute "map" in
	let text =
	  store # instantiate_layout "picture"
	    ~lang
	    [ "SRC",     lazy src;
	      "ANCHOR",  lazy ("pic" ^  string_of_int picture_nr);
	      "CAPTION", lazy caption;
	      "NUMBER",  lazy (string_of_int picture_nr);
	      "MAP",     lazy ("#map" ^ string_of_int picture_nr);
	    ]
	in
	let mapdef =
	  match map with
	      None -> ""
	    | Some mapfile ->
		let f = open_in mapfile in
		let s = transform_map f in
		close_in f;
		"<MAP NAME=\"map" ^ string_of_int picture_nr ^ "\">" ^ s ^ 
		  "</MAP>\n"
	in
	text ^ mapdef
      )
      else
	""
  end
;;


class numref =
  object (self)
    inherit shared

    method to_html ~lang store idx =
      let idref = self # node # required_string_attribute "idref" in
      let pic =
   	try idx # find idref
	with
	    Not_found ->
	      failwith ("Cannot find object with ID: " ^ idref)
      in
      let href =
      	escape_html (store # url (pic # extension # url_of_object lang)) in
      let numref_type =
	if href="" then "numref.emptyref" else "numref.default" in
      store # instantiate_layout numref_type
	~lang
	[ "HREF", lazy href;
	  "NUMBER", lazy (escape_html (pic # extension # object_number))
	]
  end
;;


class nameref =
  object (self)
    inherit shared

    method to_html ~lang store idx =
      let idref = self # node # required_string_attribute "idref" in
      let pic =
   	try idx # find idref
	with
	    Not_found ->
	      failwith ("Cannot find object with ID: " ^ idref)
      in
      let href =
      	escape_html (store # url (pic # extension # url_of_object lang)) in
      let nameref_type =
	if href="" then "nameref.emptyref" else "nameref.default" in
      store # instantiate_layout nameref_type
	~lang
	[ "HREF", lazy href;
	  "TITLE", lazy (escape_html (pic # extension # toc_title_of_object lang))
	]

  end
;;


class footnote =
  object (self)
    inherit shared

    val mutable footnote_number = 0
    val mutable text = ""

    method to_html ~lang store idx =
      let number =
	store # alloc_footnote (self : #shared :> footnote_printer) in
      let foot_anchor =
	"footnote" ^ string_of_int number in
      let text_anchor =
	"textnote" ^ string_of_int number in
      footnote_number <- number;

      (* We evaluate the footnote tree here because if there are footnotes
       * within footnotes, this is simpler to process this way.
       *)

      text <- map'n'concat
	(fun n -> n # extension # to_html ~lang store idx)
	(self # node # sub_nodes);

      store # instantiate_layout "footnote.text"
	~lang
	[ "SYMBOL", lazy (string_of_int number);
	  "TEXTANCHOR", lazy text_anchor;
	  "FOOTANCHOR", lazy foot_anchor;
	]

    method footnote_to_html ~lang store =
      let foot_anchor =
	"footnote" ^ string_of_int footnote_number in
      let text_anchor =
	"textnote" ^ string_of_int footnote_number in

      store # instantiate_layout "footnote.foot"
	~lang
	[ "SYMBOL", lazy (string_of_int footnote_number);
	  "TEXTANCHOR", lazy text_anchor;
	  "FOOTANCHOR", lazy foot_anchor;
	  "CHILDREN", lazy text;
	]

  end
;;


(**********************************************************************)

let tag_map =
  make_spec_from_alist
    ~data_exemplar:             (new data_impl (new only_data))
    ~default_element_exemplar:  (new element_impl (new no_markup))
    ~element_alist:
      [ "presentation",    (new element_impl (new presentation));
        "page",            (new element_impl (new page));
	"layout",          (new element_impl (new layout));
        "headline-layout", (new element_impl (new headline_layout));
        "hierarchy",       (new element_impl (new hierarchy));
        "plevel",          (new element_impl (new plevel));
        "sect1",           (new element_impl (new sect1));
        "sect2",           (new element_impl (new sect2));
        "sect3",           (new element_impl (new sect3));
        "title",           (new element_impl (new empty));
        "p",               (new element_impl (new p));
        "br",              (new element_impl (new br));
        "code",            (new element_impl (new code));
        "c",               (new element_impl (new c));
        "em",              (new element_impl (new em));
        "ul",              (new element_impl (new ul));
        "li",              (new element_impl (new li));
        "footnote",        (new element_impl (new footnote : #shared :> shared));
	"picture",         (new element_impl (new picture));
        "a",               (new element_impl (new a));
	"numref",          (new element_impl (new numref));
	"nameref",         (new element_impl (new nameref));
	"html",            (new element_impl (new html));
	"latex",           (new element_impl (new latex));
	"languages",       (new element_impl (new languages));
	"msg",             (new element_impl (new msg));
      ]
    ()
;;

