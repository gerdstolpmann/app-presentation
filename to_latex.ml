(* $Id$
 * ----------------------------------------------------------------------
 *
 *)


open Pxp_types
open Pxp_document
open Pxp_yacc

let map'n'concat f l =
  String.concat "" (List.map f l)
;;

let escape_latex_re = (Pcre.regexp "\\<|\\>|\\&|\\\"|\\$|\\%|\\#|\\{|\\}|\\'|\\||\\~|-|\\`|\\^|\\_|\\\\");;

let escape_latex_ws_re = Pcre.regexp "[ \t\r\n]+";;

let escape_latex ?(tt = false) s =
  (* ~tt: The typewriter fonts are ASCII-compatible, and less control sequences
   * are needed
   *)
  let s' =
    Pcre.substitute
      ~rex:escape_latex_ws_re
      ~subst:(fun s ->
		match s.[0] with
		    ' ' | '\t' -> " "
		  | '\r' | '\n' -> "\n"
		  | _ -> assert false) 
      s
  in
  let s'' =
    Pcre.substitute
      ~rex:escape_latex_re
      ~subst:
        (fun s ->
	   (* ~tt: always use \symbol *)
           match s with
(*
               "<" when not tt -> "$<${}"
             | ">" when not tt -> "$>${}"
	     | "\\" when not tt -> "$\\backslash${}"
	     | "_" when not tt -> "\\_"
	     | "{" when not tt ->"\\{"
	     | "|" when not tt ->"$\\vert${}"
	     | "}" when not tt ->"\\}"
*)
             | _ -> "\\symbol{" ^ string_of_int(Char.code s.[0]) ^ "}")
      s'
  in
  Netconversion.convert
    ~in_enc:`Enc_utf8
    ~out_enc:`Enc_usascii
    ~subst:(fun k -> 
	      (* trial to print the HTML 4 Unicode characters as much as
		 possible *)
	      match k with
		  160 -> "\\ "
		| 161 -> "!`"
		| 162 -> ""  (* CENT *)
		| 163 -> "\\pounds{}"
		| 164 -> ""  (* CURRENCY *)
		| 165 -> ""  (* YEN *)
		| 166 -> ""  (* VERT2 *)
		| 167 -> "\\S{}"
		| 168 -> "\\symbol{127}"
		| 169 -> "\\copyright{}"
		| 170 -> ""  (* ??? *)
		| 171 -> "\\symbol{19}"  (* LEFT ANGLE QUOT (T1) *)
		| 172 -> "$\\neg${}"
		| 173 -> "-{}"
		| 174 -> ""  (* REG TRADEMARK *)
		| 175 -> "$\\overline{\\;}${}"
		| 176 -> "${}^{\\circ}${}"   (* degree *)
		| 177 -> "$\\pm${}"
		| 178 -> "${}^2${}"
		| 179 -> "${}^3${}"
		| 180 -> "\\'{ }"
		| 181 -> "$\\mu${}"
		| 182 -> "\\P{}"
		| 183 -> "$\\cdot${}"
		| 184 -> "\\c{ }"
		| 185 -> "${}^1${}"
		| 186 -> ""  (* ??? *)
		| 187 -> "\\symbol{20}"  (* RIGHT ANGLE QUOT (T1) *)
		| 188 -> "$\\frac{1}{4}${}"
		| 189 -> "$\\frac{1}{3}${}"
		| 190 -> "$\\frac{3}{4}${}"
		| 191 -> "?`"
		| 192 -> "\\`{A}"
		| 193 -> "\\'{A}"
		| 194 -> "\\^{A}"
		| 195 -> "\\~{A}"
		| 196 -> "\\\"{A}"
		| 197 -> "\\AA{}"
		| 198 -> "\\AE{}"
		| 199 -> "\\c{C}"
		| 200 -> "\\`{E}"
		| 201 -> "\\'{E}"
		| 202 -> "\\^{E}"
		| 203 -> "\\\"{E}"
		| 204 -> "\\`{I}"
		| 205 -> "\\'{I}"
		| 206 -> "\\^{I}"
		| 207 -> "\\\"{I}"
		| 208 -> ""  (* ETH *)
		| 209 -> "\\~{N}"
		| 210 -> "\\`{O}"
		| 211 -> "\\'{O}"
		| 212 -> "\\^{O}"
		| 213 -> "\\~{O}"
		| 214 -> "\\\"{O}"
		| 215 -> "$\\times${}"
		| 216 -> "\\O{}"
		| 217 -> "\\`{U}"
		| 218 -> "\\'{U}"
		| 219 -> "\\^{U}"
		| 220 -> "\\\"{U}"
		| 221 -> "\\`{Y}"
		| 222 -> "\\symbol{222}" (* THORN (T1) *)
		| 223 -> "\\ss{}"
		| 224 -> "\\`{a}"
		| 225 -> "\\'{a}"
		| 226 -> "\\^{a}"
		| 227 -> "\\~{a}"
		| 228 -> "\\\"{a}"
		| 229 -> "\\aa{}"
		| 230 -> "\\ae{}"
		| 231 -> "\\c{c}"
		| 232 -> "\\`{e}"
		| 233 -> "\\'{e}"
		| 234 -> "\\^{e}"
		| 235 -> "\\\"{e}"
		| 236 -> "\\`{i}"
		| 237 -> "\\'{i}"
		| 238 -> "\\^{i}"
		| 239 -> "\\\"{i}"
		| 240 -> "\\symbol{240}"   (* eth (T1) *)
		| 241 -> "\\~{n}"
		| 242 -> "\\`{o}"
		| 243 -> "\\'{o}"
		| 244 -> "\\^{o}"
		| 245 -> "\\~{o}"
		| 246 -> "\\\"{o}"
		| 247 -> "$\\div${}"
		| 248 -> "\\o{}"
		| 249 -> "\\`{u}"
		| 250 -> "\\'{u}"
		| 251 -> "\\^{u}"
		| 252 -> "\\\"{u}"
		| 253 -> "\\`{y}"
		| 254 -> "\\symbol{254}" (* thorn (T1) *)
		| 255 -> "\\\"{y}"
		| 338 -> "\\OE{}"
		| 339 -> "\\oe{}"
		| 352 -> "\\v{S}"
		| 353 -> "\\v{s}"
		| 376 -> "\\\"{Y}"
		| 710 -> "\\symbol{94}"
		| 732 -> "\\symbol{126}"
		| 402 -> "$f${}"   (* fnof *)
		| 913 -> "$A${}"
		| 914 -> "$B${}"
		| 915 -> "$\\Gamma${}"
		| 916 -> "$\\Delta${}"
		| 917 -> "$E${}"
		| 918 -> "$Z${}"
		| 919 -> "$H${}"
		| 920 -> "$\\Theta${}"
		| 921 -> "$I${}"
		| 922 -> "$K${}"
		| 923 -> "$\\Lambda${}"
		| 924 -> "$M${}"
		| 925 -> "$N${}"
		| 926 -> "$\\Xi${}"
		| 927 -> "$O${}"
		| 928 -> "$\\Pi${}"
		| 929 -> "$P${}"
		| 931 -> "$\\Sigma${}"
		| 932 -> "$T${}"
		| 933 -> "$Y${}"
		| 934 -> "$\\Phi${}"
		| 935 -> "$X${}"
		| 936 -> "$\\Psi${}"
		| 937 -> "$\\Omega${}"
		| 945 -> "$\\alpha${}"
		| 946 -> "$\\beta${}"
		| 947 -> "$\\gamma${}"
		| 948 -> "$\\delta${}"
		| 949 -> "$\\epsilon${}"
		| 950 -> "$\\zeta${}"
		| 951 -> "$\\eta${}"
		| 952 -> "$\\theta${}"
		| 953 -> "$\\iota${}"
		| 954 -> "$\\kappa${}"
		| 955 -> "$\\lambda${}"
		| 956 -> "$\\mu${}"
		| 957 -> "$\\nu${}"
		| 958 -> "$\\xi${}"
		| 959 -> "$o${}"
		| 960 -> "$\\pi${}"
		| 961 -> "$\\rho${}"
		| 962 -> "$\\varsigma${}"
		| 963 -> "$\\sigma${}"
		| 964 -> "$\\tau${}"
		| 965 -> "$\\upsilon${}"
		| 966 -> "$\\varphi${}"
		| 967 -> "$\\chi${}"
		| 968 -> "$\\psi${}"
		| 969 -> "$\\omega${}"
		| 977 -> "$\\vartheta${}"
		| 978 -> "$\\Upsilon${}"
		| 982 -> "$\\varpi${}"
		| 8194 -> "\\hspace*{1en}"
		| 8195 -> "\\hspace*{1em}"
		| 8201 -> "\\,"
		| 8204 -> "\\mbox{}"
		| 8205 -> ""
		| 8206 -> ""
		| 8207 -> ""   (* right-to-left not supported *)
		| 8211 -> "--{}"
		| 8212 -> "---{}"
		| 8216 -> "`{}"
		| 8217 -> "'{}"
		| 8218 -> ","
		| 8220 -> "``{}"
		| 8221 -> "''{}"
		| 8222 -> ",,"
		| 8224 -> "$\\dagger${}"
		| 8225 -> "$\\ddagger${}"
		| 8226 -> "$\\bullet${}"
		| 8230 -> "\\ldots{}"
		| 8240 -> ""   (* TODO: permille sign *)
		| 8242 -> "$\\prime${}"
		| 8243 -> "$\\prime\\prime${}"
		| 8249 -> "\\symbol{14}"    (* TODO: single left-pointing angle quot mark (T1) *)  
		| 8250 -> "\\symbol{15}"    (* TODO: single right-pointing angle quot mark (T1) *)  
		| 8252 -> "$\\overline{\\;}${}"
		| 8260 -> "$/${}"
		| 8472 -> "$\\wp${}"
		| 8465 -> "$\\Im${}"
		| 8476 -> "$\\Re${}"
		| 8482 -> "${}^{\\mbox{\\sc tm}}${}"
		| 8501 -> "$\\aleph${}"
		| 8592 -> "$\\leftarrow${}"
		| 8593 -> "$\\uparrow${}"
		| 8594 -> "$\\rightarrow${}"
		| 8595 -> "$\\downarrow${}"
		| 8596 -> "$\\leftrightarrow${}"
		| 8629 -> "$\\hookleftarrow${}"
		| 8656 -> "$\\Leftarrow${}"
		| 8657 -> "$\\Uparrow${}"
		| 8658 -> "$\\Rightarrow${}"
		| 8659 -> "$\\Downarrow${}"
		| 8660 -> "$\\Leftrightarrow${}"
		| 8704 -> "$\\forall${}"
		| 8706 -> "$\\partial${}"
		| 8707 -> "$\\exists${}"
		| 8709 -> "$\\emptyset${}"
		| 8711 -> "$\\nabla${}"
		| 8712 -> "$\\in${}"
		| 8713 -> "$\\not\\in${}"
		| 8715 -> "$\\ni${}"
		| 8719 -> "$\\prod${}"
		| 8721 -> "$\\sum${}"
		| 8722 -> "$-${}"
		| 8727 -> "$\\star${}"
		| 8730 -> "$\\sqrt{\\;}${}"
		| 8733 -> "$\\propto${}"
		| 8734 -> "$\\infty${}"
		| 8736 -> "$\\angle${}"
		| 8743 -> "$\\wedge${}"
		| 8744 -> "$\\vee${}"
		| 8745 -> "$\\cap${}"
		| 8746 -> "$\\cup${}"
		| 8747 -> "$\\int${}"
		| 8756 -> "$.\\cdot.${}"
		| 8764 -> "$\\sim${}"
		| 8773 -> "$\\cong${}"
		| 8774 -> "$\\approx${}"
		| 8800 -> "$\\not=${}"
		| 8801 -> "$\\equiv${}"
		| 8804 -> "$\\leq${}"
		| 8805 -> "$\\geq${}"
		| 8834 -> "$\\subset${}"
		| 8835 -> "$\\supset${}" 
		| 8836 -> "$\\not\\subset${}"
		| 8838 -> "$\\subseteq${}" 
		| 8839 -> "$\\supseteq${}"
		| 8853 -> "$\\oplus${}"
		| 8855 -> "$\\otimes${}"
		| 8869 -> "$\\perp${}"
		| 8901 -> "$\\cdot${}"
		| 8968 -> "$\\lceil${}"
		| 8969 -> "$\\rceil${}"
		| 8970 -> "$\\lfloor${}"
		| 8971 -> "$\\rfloor${}"
		| 9001 -> "$\\langle${}"
		| 9002 -> "$\\rangle${}"
		| 9674 -> "$\\Diamond${}"
		| 9824 -> "$\\spadesuit${}"
		| 9827 -> "$\\clubsuit${}"
		| 9829 -> "$\\heartsuit${}"
		| 9830 -> "$\\diamondsuit${}"
		| _ -> ""
	   )
    s''
;;


let escape_quotes s =
  Pcre.substitute
    ~rex:(Pcre.regexp "[\\\"\\\\]")
    ~subst:
      (fun s ->
	 match s with
             "\"" -> "\\\""
	   | "\\" -> "\\\\"
	   | _    -> assert false)
    s
;;


let latexref s =
  let u = Bytes.create (String.length s * 2) in
  for k = 0 to String.length s - 1 do
    let hex = Printf.sprintf "%02x" (Char.code s.[k]) in
    Bytes.set u (k*2) hex.[0];
    Bytes.set u (k*2+1) hex.[1];
  done;
  Bytes.to_string u
;;


type lang =
    [ `Any | `Lang of string ]

type page_hierarchy =
    { pageid : string;
      pagelink : string;
      title : lang:lang -> string;
      web_path : lang:lang -> string;
      children : page_hierarchy list;
    }
;;


let string_of_lang =
  function
    | `Any -> "any"
    | `Lang s -> s



class virtual shared =
  object (self)

    (* --- default_ext --- *)

    val mutable node = (None : shared node option)

    method clone = {< >}
    method node =
      match node with
          None ->
            assert false
        | Some n -> n
    method set_node n =
      node <- Some n


    (* --- for all --- *)

    (* First, 'enumerate_pages' is invoked on all 'layout' and 'page' objects.
     * All preprocessing should happen here. After that, 'print_pages' is
     * invoked on all 'layout' and 'page' objects. The latter will
     * subsequently invoke 'to_html' to compute the page contents.
     *)

    method enumerate_pages (n : int) =
      ()

    method enumerate_pictures (n : int) =
      (* Iterates over all elements: *)
      List.fold_left
	(fun n0 x ->
	   x # extension # enumerate_pictures n0)
	n
      	(self # node # sub_nodes)

    method print_pages (out:out_channel) (lang:lang) (idx:shared index) (hier:page_hierarchy) =
      ()

    method collect_hierarchy (idx : shared index) (path : lang:lang -> string) =
      (failwith "collect_hierarchy" : page_hierarchy)

    (* --- special methods --- *)

    method refname_of_object =      (* only for objects with ID attribute *)
      (failwith "refname_of_object" : string)

    method toc_title_of_object (_ : lang) =
      (failwith "toc_title_of_object" : string)

    method object_number =
      (failwith "object_number" : string)

    method set_dirname (d:string) = 
      (failwith "set_dirname" : unit)

    method dirname =
      (self # node # root # extension # dirname : string)

    method set_option (name:string) (value:string) = 
      (failwith "set_option" : unit)

    (* --- auxiliary --- *)

    method get_page =
      (* Get the page containing this node *)
      let p = ref (self # node) in
      begin
	try
	  while !p # node_type <> T_element "page" do
	    p := !p # parent;
	  done
	with
	    Not_found -> assert false
      end;
      !p

    (* --- virtual --- *)

    method virtual to_latex : lang:lang -> (shared index) -> string

  end
;;


class only_data =
  object (self)
    inherit shared

    method to_latex ~lang idx =
      escape_latex (self # node # data)
  end
;;


class html =
  object (self)
    inherit shared

    method to_latex ~lang idx =
      ""
  end
;;


class latex =
  object (self)
    inherit shared

    method to_latex ~lang idx =
      self # node # data
  end
;;


class no_markup =
  object (self)
    inherit shared

    method to_latex ~lang idx =
      map'n'concat
	(fun n -> n # extension # to_latex ~lang idx)
	(self # node # sub_nodes)
  end
;;


class empty =
  object (self)
    inherit shared

    method to_latex ~lang idx =
      ""
  end
;;


class presentation =
  object (self)
    inherit no_markup

    val mutable dirname = "."
    val mutable use_ocamldoc = false
    val mutable front_file = None

    method set_dirname d =
      dirname <- d

    method dirname = 
      dirname

    method set_option name v =
      match name with
	  "use_ocamldoc" ->
	    use_ocamldoc <- (v = "true")
	| "front_file" ->
	    front_file <- Some v
	| _ ->
	    ()

    method enumerate_pages n =
      let rec enum k l =
	match l with
	    [] -> ()
	  | p :: l' ->
	      let k' =
		if p # node_type = T_element "page" then k+1 else k in
	      p # extension # enumerate_pages k;
	      enum k' l'
      in
      enum 0 (self # node # sub_nodes)

    method print_pages out lang idx hier =
      let rec print hier =
	let pg = 
	  try idx # find hier.pageid 
	  with
	      Not_found -> failwith("Page not found: " ^ hier.pageid) in
	pg # extension # print_pages out lang idx hier;
	List.iter print hier.children
      in

      output_string out "\\documentclass{article}\n";
      output_string out "\\usepackage[a4paper,lmargin=2cm,rmargin=2cm,tmargin=3cm,bmargin=4cm]{geometry}\n";
      output_string out "\\usepackage[T1]{fontenc}\n";
      output_string out "\\usepackage{pslatex}\n";
      output_string out "\\usepackage[pdftex]{color}\n";
      output_string out "\\usepackage{calc}\n";
      output_string out "\\usepackage{fancyhdr}\n";
      output_string out "\\usepackage{graphicx}\n";
      if use_ocamldoc then
	output_string out "\\usepackage{ocamldoc}\n";
      output_string out "\\parindent=0pt\n";
      output_string out "\\parskip=1.5ex\n";
      output_string out "\\fboxrule=2pt\n";
      output_string out "\\fboxsep=5mm\n";
      output_string out "\\newlength{\\prboxwidth}\n";
      output_string out "\\setlength{\\prboxwidth}{\\textwidth - 1cm - 8pt}\n";

      output_string out "\\newcounter{prpage}\n";
      output_string out "\\newcounter{prpic}\n";
      output_string out "\\pagestyle{fancy}\n";
      output_string out "\\lhead{\\slshape WDialog Manual}\n";
      output_string out "\\chead{}\n";
      output_string out "\\rhead{\\slshape \\rightmark}\n";
      output_string out "\\lfoot{}\n";
      output_string out "\\cfoot{\\thepage}\n";
      output_string out "\\rfoot{}\n";
      (* output_string out "\\renewcommand{\\chaptermark}[1]{}\n"; *)
      output_string out "\\renewcommand{\\sectionmark}[1]{}\n";
      output_string out "\\renewcommand{\\subsectionmark}[1]{}\n";
      output_string out "\\sloppy\n";
      output_string out "\\begin{document}\n";
      ( match front_file with
	    Some f ->
	      output_string out ("\\include{" ^ f ^ "}\n");
	  | None ->
	      ()
      );
      print hier;
      output_string out "\\end{document}\n";

    method collect_hierarchy idx path =
      let rec find_hier l =
	match l with
	    [] -> assert false
	  | x :: l' ->
	      if x # node_type = T_element "hierarchy" then
		x # extension # collect_hierarchy idx path
	      else
		find_hier l'
      in
      find_hier (self # node # sub_nodes)

  end
;;


class layout =
  object (self)
    inherit shared

    method to_latex ~lang idx =
      ""

    method print_pages out lang idx hier =
      ()

  end
;;


class headline_layout =
  object (self)
    inherit shared

    method to_latex ~lang idx =
      ""

    method print_pages out lang idx hier =
      ()
  end
;;


class hierarchy =
  object (self)
    inherit shared

    method to_latex ~lang idx = ""

    method collect_hierarchy idx path =
      (List.hd (self # node # sub_nodes)) # extension # collect_hierarchy idx path
  end
;;


class related =
  object (self)

    inherit shared

    method to_latex ~lang idx =
      ""

  end
;;


class plevel =
  object (self)
    inherit shared

    method to_latex ~lang idx = ""

    method collect_hierarchy idx path =
      let pageid =
	match self # node # attribute "idref" with
	    Value s -> s
	  | _ -> assert false
      in
      let page = 
	try
	  idx # find pageid
	with
	    Not_found ->
	      failwith ("Cannot find object with ID: " ^ pageid)
      in
      let pagelink = page # extension # refname_of_object in
      let title ~lang = page # extension # toc_title_of_object lang in
      let path' ~lang = 
	let p = path lang in
	if p = "" then title lang else p  ^ " / " ^ title lang in
      let children =
	List.flatten
	  (List.map
	     (fun n ->
		if n # node_type = T_element "plevel" then
		  [n # extension # collect_hierarchy idx path']
		else
		  []
	     )
	     (self # node # sub_nodes)) in

      { pageid = pageid;
	pagelink = pagelink;
	title = title;
	web_path = path';
	children = children;
      }
  end
;;


let title_of_node ~lang n =
  let rec find_title best_title l =
    match l with
      | n :: l' when n # node_type = (T_element "title") ->
	  ( match n # optional_string_attribute "lang" with
	      | None -> find_title n#data l'
	      | Some t ->
		  if (`Lang t) = lang then
		    n#data
		  else
		    find_title best_title l'
	  )
      | _ -> best_title
  in
  match n # optional_string_attribute "title" with
    | None ->
	find_title "" n#sub_nodes
    | Some t ->
	find_title t n#sub_nodes
;;


exception Skip_page

class page =
  object (self)
    inherit shared

    val mutable this_page = (-1)
    val mutable web_path = ""

    method to_latex ~lang idx =
      try
	(* output header *)
	let title = title_of_node ~lang self#node in
	let id =
	  match self # node # optional_string_attribute "id" with
	      None -> "ANONYMOUS"
	    | Some x -> x
	in
	let key = "page-" ^ latexref id in
	let header = 
	  "\\clearpage\n" ^
	  "\\thispagestyle{plain}\n" ^ 
	  "\\markright{" ^ escape_latex web_path ^ "}\n" ^
	  "\\fcolorbox[gray]{0.0}{0.9}{" ^
	  "\\makebox[\\prboxwidth][l]{\\huge\\sf\\bfseries " ^ escape_latex title ^ "}}\n" ^
	  "\\refstepcounter{prpage}\n" ^
	  "\\label{" ^ key ^ "}\n" ^ 
	  "\\par\\hspace*{5.1mm}{\\it Web Path: " ^ escape_latex web_path ^ "}\n"
	in

	(* process main content: *)
	let main =
          map'n'concat
	    (fun n -> n # extension # to_latex ~lang idx)
	    (self # node # sub_nodes) in

	header ^ main

      with
	  Skip_page -> ""

    method toc_title_of_object lang =
      title_of_node ~lang self#node

    method enumerate_pages n =
      this_page <- n

    method object_number =
      assert(this_page >= 0);
      string_of_int this_page

    method print_pages out lang idx hier =
      let do_print = 
	self # node # required_string_attribute "for-latex" = "yes" in
      web_path <- hier.web_path ~lang;
      if do_print then begin
	output_string out (self # to_latex ~lang idx);
	output_string out "\n%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%\n";
      end

    method refname_of_object =
      match self # node # attribute "id" with
	  Value s -> 
	    "page-" ^ latexref s
	| Implied_value -> 
	    "page-UNKNOWN"
	| _ -> assert false
  end
;;


class section the_tag =
  object (self)
    inherit shared

    val tag = the_tag

    method to_latex ~lang idx =
      let title = title_of_node ~lang self # node in
      let children =
	map'n'concat
	  (fun n -> n # extension # to_latex ~lang idx)
	  self#node#sub_nodes
      in
      let latex_cmd =
	match tag with
	    "sect1" -> "\\section"
	  | "sect2" -> "\\subsection"
	  | "sect3" -> "\\subsubsection" 
	  | _ -> assert false in
      latex_cmd ^ "{" ^ title ^ "}\n" ^ children
  end
;;

class sect1 = section "sect1";;
class sect2 = section "sect2";;
class sect3 = section "sect3";;


class map_tag pre_cmd post_cmd =
  object (self)
    inherit shared

    method private language =
      (* Determine the language of the element *)
      match self # node # optional_string_attribute "lang" with
	| None -> `Any
	| Some l -> `Lang l

    method to_latex ~lang idx =
      let taglang = self # language in
      if taglang = `Any || taglang = lang then (
	let children =
	  map'n'concat
	    (fun n -> n # extension # to_latex ~lang idx)
	    (self # node # sub_nodes)
	in
	pre_cmd ^ children ^ post_cmd
      )
      else ""
  end
;;

class p = map_tag "\n\n" "\n\n";;
class em = map_tag "{\\it " "}";;
class ul = map_tag "\n\\begin{itemize}\n" "\n\\end{itemize}\n";;
class li = map_tag "\n\\item{}" "";;
class br = map_tag "\\mbox{}\\\\\\relax{}\n" "";;
class footnote = map_tag "\\footnote{" "}";;


class code =
  object (self)
    inherit shared

    method to_latex ~lang idx =
      let data = self # node # data in
      (* convert tabs *)
      let l = String.length data in
      let rec preprocess i0 i column =
	(* this is very ineffective but comprehensive: *)
	if i < l then
	  match data.[i] with
	      '\t' ->
		let n = 8 - (column mod 8) in
		let s = String.make n '~' in 
		escape_latex ~tt:true (String.sub data i0 (i-i0)) ^
		s ^ preprocess (i+1) (i+1) (column + n)
	    | ' ' ->
		escape_latex ~tt:true (String.sub data i0 (i-i0)) ^
		"\\ " ^ preprocess (i+1) (i+1) (column + 1)
	    | '\n' ->
		escape_latex ~tt:true (String.sub data i0 (i-i0)) ^
		"\\mbox{}\\\\\\mbox{}\n" ^ preprocess (i+1) (i+1) 0
	    | c ->
		preprocess i0 (i+1) (column + 1)
	else
	  escape_latex ~tt:true (String.sub data i0 (i-i0))
      in
      "\\begin{quote}\n" ^
      "\\tt\\mbox{}\n" ^
      preprocess 0 0 0 ^
      "\\end{quote}\n"
  end
;;

class c =
  object (self)
    inherit shared

    method to_latex ~lang idx =
      let data = self # node # data in
      (* convert tabs and spaces *)
      let l = String.length data in
      let rec preprocess i0 i =
	(* this is very ineffective but comprehensive: *)
	if i < l then
	  match data.[i] with
	      '\t' | ' ' ->
		escape_latex ~tt:true (String.sub data i0 (i-i0)) ^
		"\\ " ^ preprocess (i+1) (i+1)
	    | '\n' ->
		escape_latex ~tt:true (String.sub data i0 (i-i0)) ^
		"\\mbox{}\\\\\\mbox{}\n" ^ preprocess (i+1) (i+1)
	    | c ->
		preprocess i0 (i+1)
	else
	  escape_latex ~tt:true (String.sub data i0 (i-i0))
      in
      "{\\tt " ^ preprocess 0 0 ^ "}"
  end
;;


class a =
  object (self)
    inherit shared

    method to_latex ~lang idx =
      let _langref =
	match self # node # optional_string_attribute "langref" with
	  | None -> lang
	  | Some l -> (`Lang l)
      in
      let href =
	match self # node # attribute "href" with
	    Value v -> 
	      "\\footnote{(URL: " ^ escape_latex v ^ ")}"
	  | Valuelist _ -> assert false
	  | Implied_value ->
	      begin match self # node # attribute "idref" with
		  Value v ->
		    let obj =
		      try idx # find v
		      with
			  Not_found ->
			    failwith ("Cannot find object with ID: " ^ v)
		    in
		    let key = obj # extension # refname_of_object in
		    " ($\\rightarrow$ \\pageref{" ^ key ^ "})"
		| Valuelist _ -> assert false
		| Implied_value ->
		    (* Link to this page: *)
		    let p = self # get_page in
		    let key = p # extension # refname_of_object in
		    " ($\\rightarrow$ \\pageref{" ^ key ^ "})"
	      end
      in
      "{\\it " ^ escape_latex(self # node # data) ^ "}" ^ href

  end
;;


class picture =
  object (self)
    inherit shared

    val mutable picture_nr = (-1)     (* not yet specified *)

    method object_number =
      assert( picture_nr >= 1);
      string_of_int picture_nr

    method refname_of_object =
      let page = self # get_page in
      let pageref = page # extension # refname_of_object in
      pageref ^ "-pic-" ^ string_of_int picture_nr

     method private language =
      (* Determine the language of the picture *)
      match self # node # optional_string_attribute "lang" with
	| None -> `Any
	| Some l -> `Lang l

   method toc_title_of_object _ =
      self # node # required_string_attribute "caption"

    method enumerate_pictures n =
      picture_nr <- n;
      n + 1

    method to_latex ~lang idx = 
      let plang = self # language in 
      if plang = `Any || plang = lang then (
	let page = self # get_page in
	let pageref = page # extension # refname_of_object in
	let key = pageref ^ "-pic-" ^ string_of_int picture_nr in
	let header = 
	  "\\refstepcounter{prpic}\n" ^
	    "\\label{" ^ key ^ "}"
	in
	let src = self # node # required_string_attribute "src" in
	let caption = self # node # required_string_attribute "caption" in
	let filename = Filename.chop_extension 
	  (Filename.concat (self # dirname) src) ^ ".pdf" in
	let s =
	  "\\begin{figure}[bp]\n" ^
	    "\\fboxsep=2mm\n" ^
	    "\\raggedleft\n" ^ 
	    "\\fcolorbox[gray]{0.0}{0.9}{\\begin{minipage}[l]{5cm}\n" ^
	    "{\\bfseries Picture " ^ string_of_int picture_nr ^ ":} " ^
	    escape_latex caption ^
	    "\\end{minipage}}\n" ^ 
	    "\\vspace{1.5ex}\n" ^
	    "\\includegraphics[width=\\textwidth]{" ^ filename ^ "}\n" ^
	    "\\end{figure}\n" 
	in
	header ^ s
      )
      else ""
  end
;;


class numref =
  object (self)
    inherit shared

    method to_latex ~lang idx =
      let idref = self # node # required_string_attribute "idref" in
      let obj =
	try idx # find idref
	with
	    Not_found ->
	      failwith ("Cannot find object with ID: " ^ idref)
      in
      escape_latex (obj # extension # object_number)
  end
;;


class nameref =
  object (self)
    inherit shared

    method to_latex ~lang idx =
      let idref = self # node # required_string_attribute "idref" in
      let obj =
	try idx # find idref
	with
	    Not_found ->
	      failwith ("Cannot find object with ID: " ^ idref)
      in
      let key = obj # extension # refname_of_object in
      let href = " ($\\rightarrow$ \\pageref{" ^ key ^ "})" in
      let link = obj # extension # toc_title_of_object lang in

      "{\\it " ^ escape_latex link ^ "}" ^ href
  end
;;


(**********************************************************************)

let tag_map =
  make_spec_from_alist
    ~data_exemplar:             (new data_impl (new only_data))
    ~default_element_exemplar:  (new element_impl (new no_markup))
    ~element_alist:
      [ "presentation",    (new element_impl (new presentation));
        "page",            (new element_impl (new page));
	"layout",          (new element_impl (new layout));
        "headline-layout", (new element_impl (new headline_layout));
        "hierarchy",       (new element_impl (new hierarchy));
        "plevel",          (new element_impl (new plevel));
        "sect1",           (new element_impl (new sect1));
        "sect2",           (new element_impl (new sect2));
        "sect3",           (new element_impl (new sect3));
        "title",           (new element_impl (new empty));
        "p",               (new element_impl (new p));
        "br",              (new element_impl (new br));
        "code",            (new element_impl (new code));
        "c",               (new element_impl (new c));
        "em",              (new element_impl (new em));
        "ul",              (new element_impl (new ul));
        "li",              (new element_impl (new li));
        "footnote",        (new element_impl (new footnote));
	"picture",         (new element_impl (new picture));
        "a",               (new element_impl (new a));
	"numref",          (new element_impl (new numref));
	"nameref",         (new element_impl (new nameref));
	"html",            (new element_impl (new html));
	"latex",           (new element_impl (new latex));
	"languages",       (new element_impl (new empty));
	"msg",             (new element_impl (new empty));

      ]
    ()
;;

